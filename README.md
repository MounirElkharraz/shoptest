# How to use Docker






### First step : Build the Docker Image
Build the Docker Image: Open a terminal, 
navigate to the directory containing the Dockerfile, 
and run the following command to build the Docker image:

docker build -t de.imedia24:0.0.1-SNAPSHOT .

### Second step:Run the Docker Container
Run the Docker Container: Once the image is built,
you can run a Docker container based on that image. Use the following command:

docker run -p 8080:8080 de.imedia24:0.0.1-SNAPSHOT

### Third step: Access Your Application
With the container running, you can access test all the restAPI in the 
application 
by opening a web browser and navigating to http://localhost:8080
(assuming your application runs on port 8080). 
Replace localhost with the appropriate hostname or IP address if necessary.
