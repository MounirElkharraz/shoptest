package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.UpdateProductRequest
import de.imedia24.shop.utils.Utils
import javassist.NotFoundException
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProductService(private val productRepository: ProductRepository,
    private val util: Utils
) {

    fun findProductBySku(sku: String): ProductResponse? {
        val productEntity = productRepository.findBySku(sku)

        return if (productEntity != null) {
            productEntity.toProductResponse()
        } else {
            null // Return null when productEntity is null
        }
    }


    fun addProduct(productRequest: ProductRequest): ProductResponse {

        val productEntity = productRequest.toProductEntity()
        productEntity.createdAt=util.getCurrentZonedDateTime()
        if (productEntity != null) {

            val savedProductEntity = productRepository.save(productEntity)

            if (savedProductEntity != null) {
                return savedProductEntity.toProductResponse()
            } else {
                throw IllegalStateException("Failed to save the product entity.")
            }
        } else {
            throw IllegalArgumentException("Failed to create the product entity from the request.")
        }
    }

    fun findAllProductsBySku(sku:List<String>):List<ProductResponse>{

        val products= productRepository.findAllBySkuIn(sku)

        val productResponses = products.map { productEntity ->
            util.mapProductEntityToProductResponse(productEntity)
        }

        return productResponses
    }

    fun findAllProducts():List<ProductResponse>{

        val allProducts= productRepository.findAll()
        val productResponses = allProducts.map { productEntity ->
           util.mapProductEntityToProductResponse(productEntity)
        }
        return productResponses

    }


    fun updateProduct(sku: String, updateRequest: UpdateProductRequest): ProductResponse {

        val existingProduct = productRepository.findBySku(sku)
            ?: throw NotFoundException("Product with SKU $sku not found")

        updateRequest.name.let { existingProduct.name = it }
        updateRequest.description.let { existingProduct.description = it }
        updateRequest.price.let { existingProduct.price = it }



        return productRepository.save(existingProduct)!!.toProductResponse()
    }

}

