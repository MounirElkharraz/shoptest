package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column

data class UpdateProductRequest(
    val name: String,
    val description: String,
    val price: BigDecimal,

) {
    companion object {
        fun ProductEntity.toProductResponse() = UpdateProductRequest(
            name = name,
            description = description ?: "",
            price = price,


        )
    }
}
