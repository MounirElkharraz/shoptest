package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column

data class ProductResponse(
    val sku: String,
    val name: String,
    val description: String,
    val price: BigDecimal,
    var createdAt: ZonedDateTime? = null,
    val updatedAt: ZonedDateTime? = null,
    var quantityInStock: Int?=null,
    var availabilityStatus: String?=null
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price,
            createdAt=createdAt,
            updatedAt=updatedAt,
            quantityInStock=quantityInStock,
            availabilityStatus=availabilityStatus

        )
    }
}
