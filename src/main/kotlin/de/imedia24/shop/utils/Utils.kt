package de.imedia24.shop.utils

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import org.springframework.stereotype.Component
import java.time.ZoneId
import java.time.ZonedDateTime

@Component
class Utils {
    fun getCurrentZonedDateTime(): ZonedDateTime {
        return ZonedDateTime.now(ZoneId.of("UTC"))
    }

    fun mapProductEntityToProductResponse(productEntity: ProductEntity): ProductResponse {
        val productResponse = ProductResponse(
            sku = productEntity.sku,
            name = productEntity.name,
            description = productEntity.description.toString(),
            price = productEntity.price,
            quantityInStock=productEntity.quantityInStock,
            availabilityStatus=productEntity.availabilityStatus
        )

        return productResponse
    }
}