package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.UpdateProductRequest
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/products")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/get_by_sku/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")
        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @PostMapping("/add", produces = ["application/json;charset=utf-8"])
    fun addProduct(@RequestBody productRequest: ProductRequest): ResponseEntity<Any> {
        logger.info("Request to add product $productRequest")

        val product = productService.addProduct(productRequest)
        return  ResponseEntity.ok(product)

    }

    @GetMapping("/get_by",produces = ["application/json;charset=utf-8"])
    fun getProductsBySkus(@RequestParam skus: List<String?>?): ResponseEntity<List<ProductResponse>> {

        if (skus.isNullOrEmpty()) {
            return ResponseEntity.badRequest().body(emptyList())
        }

        val products = productService.findAllProductsBySku(skus.filterNotNull())

        return if (products.isEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @GetMapping("/all")
    fun getAllProducts(): ResponseEntity<List<ProductResponse>> {

        val products = productService.findAllProducts()

        return if (products.isEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/update/{sku}")
    fun updateProduct(@PathVariable sku: String,
                      @RequestBody updateRequest: UpdateProductRequest
    ):
            ResponseEntity<ProductResponse> {
        val updatedProduct = productService.updateProduct(sku, updateRequest)
        return ResponseEntity.ok(updatedProduct)
    }


}
