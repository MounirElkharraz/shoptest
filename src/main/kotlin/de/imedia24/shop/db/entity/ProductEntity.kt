package de.imedia24.shop.db.entity

import de.imedia24.shop.domain.product.ProductResponse
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
data class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String = "",

    @Column(name = "name", nullable = false)
    var name: String = "",

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "price", nullable = false)
    var price: BigDecimal = BigDecimal.ZERO,

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    var createdAt: ZonedDateTime? = null,

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    val updatedAt: ZonedDateTime? = null,

    @Column(name = "quantity_in_stock")
    var quantityInStock: Int?=null,

    @Column(name = "availability_status")
    var availabilityStatus: String?=null
) {

    companion object {
        fun ProductEntity.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price,
            createdAt=createdAt,
            updatedAt=updatedAt,
            quantityInStock=quantityInStock,
            availabilityStatus=availabilityStatus,


        )
    }
}
