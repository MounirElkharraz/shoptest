package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.UpdateProductRequest
import org.junit.jupiter.api.AfterEach

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.util.*

@WebMvcTest(ProductController::class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension::class)

class ProductControllerTests() {

	@Autowired
	private lateinit var mockMvc: MockMvc

	@MockBean
	private lateinit var productService: ProductController

	@Autowired
	private lateinit var objectMapper: ObjectMapper
	@MockBean
	private lateinit var productRepository : ProductRepository


	private val sku = "ABC129"



	@BeforeEach
	fun setUp() {
		MockitoAnnotations.openMocks(this)

		val productRequest = ProductRequest("ABC135","Product Name30"
			,"Product Description30",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		val productRequest2 = ProductRequest("ABC140","Product Name40"
			,"Product Description40",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		val productRequest3 = ProductRequest("ABC150","Product Name50"
			,"Product Description50",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		val productEntities = listOf(
			productRequest.toProductEntity(),
			productRequest2.toProductEntity(),
			productRequest3.toProductEntity()
		)

		`when`(productRepository.saveAll(productEntities)).thenReturn(productEntities)
	}

	@AfterEach
	fun afterEach() {
		productRepository.deleteAll()
	}

	@Test
	fun testFindProductBySku() {
		// Mock the behavior of your productService.findProductBySku method
		val productResponse = ProductResponse("ABC129","Product Name5"
				,"Product Description",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		`when`(productService.findProductBySku(sku)).thenReturn( ResponseEntity.ok(productResponse))

		mockMvc.perform(
			MockMvcRequestBuilders.get("/products/get_by_sku/ABC129")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk)
			.andExpect(content().json(objectMapper.writeValueAsString(productResponse)))
	}

	@Test
	fun testAddProduct(){
		val productRequest = ProductRequest("ABC130","Product Name30"
			,"Product Description30",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		val productResponse = ProductResponse("ABC130","Product Name30"
			,"Product Description30",BigDecimal("19.99"),null,null,
			4,"availabilityStatus")
		`when`(productService.addProduct(productRequest)).thenReturn( ResponseEntity.ok(productResponse))

		mockMvc.perform(
			MockMvcRequestBuilders.post("/products/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(productRequest))
		)
			.andExpect(MockMvcResultMatchers.status().isOk)
			.andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(productResponse)))

	}

	@Test
	fun testGetProductsBySkus() {
		// Mock the productService.findAllProductsBySku() method
		val skus = listOf("ABC135", "ABC140", "ABC150")
		val products = listOf(
			ProductResponse("ABC135","Product Name30"
				,"Product Description30",BigDecimal("19.99"),null,null,
				4,"availabilityStatus"),
			ProductResponse("ABC140","Product Name40"
				,"Product Description40",BigDecimal("19.99"),null,null,
				4,"availabilityStatus"),
			ProductResponse("ABC150","Product Name50"
				,"Product Description50",BigDecimal("19.99"),null,null,
				4,"availabilityStatus")
		)

		`when`(productService.getProductsBySkus(skus)).thenReturn(ResponseEntity.ok(products))

		// Perform the HTTP GET request with query parameters
		mockMvc.perform(MockMvcRequestBuilders.get("/products/get_by")
			.param("skus", "ABC135", "ABC140", "ABC150"))
			.andExpect(MockMvcResultMatchers.status().isOk)
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			.andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(products)))

	}

	@Test
	fun testUpdateProduct() {
		val sku2 = "ABC135"
		val name = "update_name"
		val description = "update_description"
		val price = BigDecimal("7")

		val updateRequest = UpdateProductRequest(name,description,price)
		val expectedResponse = ProductResponse(sku2,name
			,description,BigDecimal("7"),null,null,
			4,"availabilityStatus")

		// Mock the productService.updateProduct method
		`when`(productService.updateProduct(sku, updateRequest)).thenReturn(ResponseEntity.ok(expectedResponse))

		val resultActions: ResultActions = mockMvc.perform(
			MockMvcRequestBuilders.post("/products/update/$sku")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(updateRequest))
		)

		resultActions
			.andExpect(MockMvcResultMatchers.status().isOk)
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(expectedResponse)))


	}


}
