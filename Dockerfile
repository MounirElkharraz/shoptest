FROM openjdk:8-jre-alpine
WORKDIR /app
COPY build/libs/shop-0.0.1-SNAPSHOT.jar .
CMD ["java", "-jar", "shop-0.0.1-SNAPSHOT.jar"]
